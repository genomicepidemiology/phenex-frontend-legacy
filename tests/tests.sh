#!/usr/bin/env ash

# Check if syntax is ok
status=$(nginx -t 2>&1 >/dev/null | grep 'syntax is ok')
if [ -z "$status" ]; then
  echo "Error: Nginx invalid syntax in nginx.conf"
  exit 1
fi

# Check if nginx passes config test
status=$(nginx -t 2>&1 >/dev/null | grep "test is successful")
if [ -z "$status" ]; then
  echo "Error: Nginx test failed for nginx.conf"
  exit 1
fi

# Check if enginx is running
status=$(netstat -tulpn | grep "nginx")
if [ -z "$status" ]; then
  echo "Error: Nginx is not running"
  exit 1
fi

# Install nodejs modules
yarn install

# Check if app is minified
gulp --gulpfile scripts/gulpfile.js --production
status=$(curl -s localhost | grep "app.min")
if [ -z "$status" ]; then
  echo "Error: Application is not minified!"
  exit 1
fi
