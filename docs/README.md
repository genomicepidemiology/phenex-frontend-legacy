# JavaScript

JavaScript is written in ECMA Script 6.

# Bootstrap

* [Theming Bootstrap](https://getbootstrap.com/docs/4.3/getting-started/theming/)

# SASS

* [Structuring your Sass Projects](https://itnext.io/structuring-your-sass-projects-c8d41fa55ed4)
* [SASS Project Structure for Big Projects](https://medium.com/@dannyhuang_75970/sass-project-structure-for-big-projects-8c4a740846ee)

# Docker

```bash
# Build development image manually
$ docker build --build-arg BUILD_DATE=$(date -u +'%d-%m-%YT%H:%M:%S') \
  --build-arg GIT_COMMIT=0 \
  --build-arg DEBUG=false \
  -t genomicepidemiology/phenex-frontend:dev \
  -f docker/Dockerfile.production .

# Build testing image manually
$ docker build -t genomicepidemiology/phenex-frontend \
  --build-arg BUILD_DATE=$(date -u +'%d-%m-%YT%H:%M:%S') \
  --build-arg GIT_COMMIT=0 \
  --build-arg DEBUG=false \
  -t genomicepidemiology/phenex-frontend:test \
  -f docker/Dockerfile.testing .

# Build production image manually
$ docker build -t genomicepidemiology/phenex-frontend \
  --build-arg BUILD_DATE=$(date -u +'%d-%m-%YT%H:%M:%S') \
  --build-arg GIT_COMMIT=0 \
  --build-arg DEBUG=false \
  -t genomicepidemiology/phenex-frontend:prod \
  -f docker/Dockerfile.production .

# Run images
$ docker run --name phenex_dev_frontend --rm -d genomicepidemiology/phenex-frontend:dev
$ docker run --name phenex_test_frontend --rm -d genomicepidemiology/phenex-frontend:test
$ docker run --name phenex_frontend --rm -d genomicepidemiology/phenex-frontend:prod
```

# Docker Composer

```bash
# Build images
$ docker-compose -f composer-development.yml build
$ docker-compose -f composer-testing.yml build
$ docker-compose -f composer-production.yml build

# Run project in front
$ docker-compose -f composer-development.yml up
$ docker-compose -f composer-testing.yml up
$ docker-compose -f composer-production.yml up

# Run project in background
$ docker-compose -f composer-development.yml up -d
$ docker-compose -f composer-testing.yml up -d
$ docker-compose -f composer-production.yml up -d
```

# Testing

Once image is running then, run tests.sh script
```bash
$ . tests/tests.sh
```
