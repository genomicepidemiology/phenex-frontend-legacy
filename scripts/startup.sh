#!/usr/bin/env ash

## NGINX
rm -f /etc/nginx/conf.d/default.conf
cp config/services/nginx.conf /etc/nginx/nginx.conf
ln -s /usr/share/nginx/html/config/services/app.nginx.${DEPLOYMENT}.conf /etc/nginx/conf.d/app.conf
ln -s /usr/share/nginx/html/scripts/aliases.sh /etc/profile.d/aliases.sh

if [ "$DEPLOYMENT" = "development" ]; then
  ## NodeJS
  yarn install
  gulp --gulpfile scripts/gulpfile.js --${DEPLOYMENT}
fi

## START WEB_SERVER IN FOREGROUND
nginx -g "daemon off;"
