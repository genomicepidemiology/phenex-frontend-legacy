// these packages were installed via npm
const del = require('del');
const argv = require('yargs').argv;
const gulp = require('gulp');
const sass = require('gulp-sass');
const babel = require("gulp-babel");
const cache = require('gulp-cache');
const gif = require('gulp-if');
const rename = require('gulp-rename');
const concat = require('gulp-concat');
const sourcemaps = require("gulp-sourcemaps");
const cssnano = require('gulp-cssnano');
const browserSync = require('browser-sync');
const plumber = require('gulp-plumber');
const bro = require('gulp-bro');
const babelify = require('babelify');

var VUE = [
  '../app/vuejs/*.js',
  '../app/vuejs/**/*.js',
  '../app/vuejs/**/**/*.js',
  '../app/vuejs/*.html',
  '../app/vuejs/**/html/*.html',
  '../app/vuejs/**/**/html/*.html',
  '../app/vuejs/**/**/**/html/*.html',
]

var JQUERY = [
  '../app/jquery/*.js',
]

var ASSETS = [
  '../app/assets/*',
  '../app/assets/**/*',
  '../app/assets/**/**/*',
  '../static/../app/assets/img',
]

var SCSS = [
  '../app/scss/*.scss',
  '../app/scss/**/*.scss',
  '../app/scss/**/**/*.scss',
]

var HTML = [
  '../app/html/*.html',
  '../app/html/**/*.html',
  '../app/html/**/**/*.html',
]

var STATIC_ROOT_PATH = '../static';
var STATIC_ASSETS_PATH = '../static/assets';
var STATIC_FONTS_PATH = '../static/fonts';
var STATIC_JS_PATH = '../static/js';
var STATIC_CSS_PATH = '../static/css';
var STATIC_HTML_PATH = '../static/html';

var WATCHED_RESOURCES = [...JQUERY, ...VUE, ...SCSS, ...HTML];

// Delete files in static dir
function clearResources() {
  return del('../static/*', {
    force: true
  });
};

// Copy filese at resource path(s)
function loadResources(callback) {
  // index.html
  if (argv.production) {
    var INDEX = '../app/html/index.production.html';
  } else if (argv.testing) {
    var INDEX = '../app/html/index.testing.html';
  } else if (argv.development) {
    var INDEX = '../app/html/index.development.html';
  } else if (argv.construction) {
    var INDEX = '../app/html/index.construction.html';
  } else {
    var INDEX = '../app/html/index.development.html';
  }

  // rename to index.html
  gulp.src(INDEX).pipe(rename('index.html')).pipe(gulp.dest(STATIC_ROOT_PATH));

  var RESOURCES = {
    'js': {
      'production': [
        '../node_modules/jquery/dist/jquery.min.js',
        '../node_modules/bootstrap/dist/js/bootstrap.min.js',
        '../node_modules/d3/d3.min.js',
        '../node_modules/topojson/build/topojson.min.js',
        '../node_modules/datamaps/dist/datamaps.world.min.js',
        '../node_modules/vue/dist/vue.min.js',
      ],
      'development': [
        '../node_modules/jquery/dist/jquery.js',
        '../node_modules/bootstrap/dist/js/bootstrap.js',
        '../node_modules/d3/d3.js',
        '../node_modules/topojson/build/topojson.js',
        '../node_modules/datamaps/dist/datamaps.world.min.js',
        '../node_modules/vue/dist/vue.js',
        '../node_modules/vee-validate/dist/vee-validate.min.js',
      ]
    },
    'css': {
      'production': [
        '../node_modules/font-awesome/css/font-awesome.min.css',
        '../node_modules/hover.css/css/hover-min.css',
      ],
      'development': [
        '../node_modules/font-awesome/css/font-awesome.css',
        '../node_modules/hover.css/css/hover.css',
      ]
    },
    'fonts': [
      '../node_modules/font-awesome/fonts/*',
    ],
    'maps': {
      'css': [
        '../node_modules/hover.css/css/hover.css.map',
        '../node_modules/bootstrap/dist/css/bootstrap.css.map',
        '../node_modules/font-awesome/css/font-awesome.css.map',
      ],
      'js': [
        '../node_modules/bootstrap/dist/js/bootstrap.js.map',
      ]
    },
    'app': {
      'js': {
        'vuejs': [
          '../app/vuejs/app.es6.js',
          '../app/vuejs/utilities/regex.es6.js',
          '../app/vuejs/components/navbar/navbar.es6.js',
        ],
        'jquery': [
          '../app/jquery/app.js',
        ],
      },
      'html': [
        '../app/html/elements/navbar.html',
      ]
    }
  }

  gulp.src(RESOURCES['fonts']).pipe(gulp.dest(STATIC_FONTS_PATH));
  gulp.src(RESOURCES['app']['html']).pipe(gulp.dest(STATIC_HTML_PATH));

  gulp.src(RESOURCES['app']['js']['vuejs'][0])
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(bro({transform: [babelify.configure({ presets: ['@babel/preset-env'] })] }))
		.pipe(babel({
      presets: ['@babel/preset-env'],
      comments: (argv.production || argv.testing) ? false : true,
      compact: (argv.production || argv.testing) ? true : false,
    }))
    .pipe(gif(argv.production, rename('vue.app.min.js'), rename('vue.app.js')))
		.pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(STATIC_JS_PATH));

  gulp.src(RESOURCES['app']['js']['jquery'])
    .pipe(plumber())
    .pipe(concat('concat.js'))
    .pipe(gif(argv.production, rename('jquery.app.min.js'), rename('jquery.app.js')))
    .pipe(gulp.dest(STATIC_JS_PATH));

  var libs_js = (argv.production) ? RESOURCES['js']['production'] : RESOURCES['js']['development'];
  gulp.src(libs_js)
    .pipe(plumber())
    .pipe(gif(argv.production || argv.testing, concat('concat.js')))
    .pipe(gif(argv.production, rename('vendors.min.js')))
    .pipe(gif(argv.testing, rename('vendors.js')))
    .pipe(gulp.dest(STATIC_JS_PATH));

  var libs_css = (argv.production) ? RESOURCES['css']['production'] : RESOURCES['css']['development'];
  gulp.src(libs_css)
    .pipe(plumber())
    .pipe(sass().on('error', sass.logError))
    .pipe(gif(argv.production || argv.testing, concat('concat.css')))
    .pipe(gif(argv.production, cssnano()))
    .pipe(gif(argv.production, rename('vendors.min.css')))
    .pipe(gif(argv.testing, rename('vendors.css')))
    .pipe(gulp.dest(STATIC_CSS_PATH));

  var libs_maps_js = RESOURCES['maps']['js'];
  var libs_maps_css = RESOURCES['maps']['css'];
  gulp.src(libs_maps_js).pipe(gulp.dest(STATIC_JS_PATH));
  gulp.src(libs_maps_css).pipe(gulp.dest(STATIC_CSS_PATH));

  gulp.src(SCSS)
    .pipe(plumber())
    .pipe(sass().on('error', sass.logError))
    .pipe(gif(argv.production, cssnano()))
    .pipe(gif(argv.production, rename('styles.min.css'), rename('styles.css')))
    .pipe(gulp.dest(STATIC_CSS_PATH));

  cache.clearAll();
  callback();
}

// Reload browser on resource change
function syncBrowser(callback) {

  browserSync.init({
    proxy: 'http://localhost/',
    port: 3001,
    open: false,
  });

  gulp.watch(WATCHED_RESOURCES, gulp.series(loadResources)).on('change', browserSync.reload);

  callback();
}

// Load resorces on change
function changedResources(callback) {
  gulp.watch(WATCHED_RESOURCES, loadResources);
  callback();
}

exports.clear = clearResources;
exports.load = loadResources;
exports.watch = changedResources;
exports.sync = gulp.series(syncBrowser);
exports.default = gulp.series(clearResources, loadResources);
