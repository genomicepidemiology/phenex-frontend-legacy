"use strict"

import Vue from 'vue/dist/vue.js';
import VeeValidate from 'vee-validate';
import Axios from 'axios';
import VueAxios from 'vue-axios';
import Store from './components/store.es6.js';
import Storage from './utilities/storage.es6.js';
import Navbar from './components/navbar/navbar.es6.js';

Vue.config.productionTip = true;
Vue.prototype.$appName = 'Phenex Frontend';

Vue.use(VueAxios, Axios);

loggInFromStorage();

function loggInFromStorage() {
  let accessToken = Storage.getCookie('acctkn');
  if (accessToken) {
    console.log("Reading From Storage");
    Vue.prototype.axios.defaults.headers.common['Authorization'] = `Bearer ${accessToken}`;

    Store.commit('setUserLoggedInFlag');
    Store.commit('setAccessToken', accessToken);

    let jwt_payload = accessToken.split('.')[1];
    let json_payload_str = atob(jwt_payload);
    let user_data = JSON.parse(json_payload_str);

    Store.commit('setUserData', user_data.identity);
  }
}
