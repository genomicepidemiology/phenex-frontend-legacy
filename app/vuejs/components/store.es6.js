"use strict"

/*
 * Motivation behind STORE object
 * see: https://vuex.vuejs.org/
 * see: https://vuex.vuejs.org/guide/
 * see: https://scrimba.com/p/pnyzgAP/cMPa2Uk
 */

import Vue from 'vue/dist/vue.js';
import Vuex from 'vuex/dist/vuex.js'

Vue.use(Vuex);

export default new Vuex.Store({
  strict: false,
  state: {
    user: {
      data: {},
      isLoggedIn: false,
      accessToken: '',
    },
  },
  getters: {
    data: function(state) {
      return state.user.data;
    }
  },
  mutations: {
    setUserData: function(state, data) {
      state.user.data = data;
    },
    setAccessToken: function(state, token) {
      state.user.accessToken = token;
    },
    setUserLoggedInFlag: function(state, flag = true) {
      state.user.isLoggedIn = flag;
    },
  }
});
