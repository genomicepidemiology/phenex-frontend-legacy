"use strict"

import Vue from 'vue/dist/vue.js';
import VeeValidate from 'vee-validate';
import Store from '../store.es6.js';
import Regex from '../../utilities/regex.es6.js';
import { mapState } from 'vuex/dist/vuex.js';

Vue.use(VeeValidate);

let navbar = new Vue({
  el: '#navbar',
  data: {
    name: null,
    store: Store,
    form: {
      input: {
        email: '',
        password: '',
        password_confirm: '',
        accept_tnc: true,
      },
      flag: {
        email: true,
        password: true,
        password_confirm: false,
        sign_in: false,
        submit: false,
      },
      alert_type: 'alert-success',
      submit_text: 'Sign In',
      submit_msg: '',
    }
  },
  watch: {
    "form.input.email": function() {
      this.form.flag.submit = Regex.is_valid_email(this.form.input.email);
    },
    "form.input.password": function() {
      this.form.flag.submit = false;
      if (this.form.input.password.length >= 5 && this.form.input.password_confirm.length >= 5) {
        this.form.flag.submit = this.form.input.password == this.form.input.password_confirm;
      }

      let noInputAndValidEmail = ((this.form.input.password.length == 0 && this.form.input.password_confirm.length == 0) && this.form.input.email)
      if (noInputAndValidEmail) {
        this.form.flag.submit = Regex.is_valid_email(this.form.input.email);
      }
    },
    "form.input.password_confirm": function() {
      this.form.flag.submit = false;
      if (this.form.input.password.length >= 5 && this.form.input.password_confirm.length >= 5) {
        this.form.flag.submit = this.form.input.password == this.form.input.password_confirm;
      }

      let noInputAndValidEmail = ((this.form.input.password.length == 0 && this.form.input.password_confirm.length == 0) && this.form.input.email)
      if (noInputAndValidEmail) {
        this.form.flag.submit = Regex.is_valid_email(this.form.input.email);
      }
    }
  },
  methods: {
    show_navbar_alert: function(message = '', alert_type = 'alert-success') {
      this.form.submit_msg = message;
      $('#navbarAlert span').attr('aria-hidden', 'false');
      $('#navbarAlert').removeClass(['alert-danger', 'alert-success', 'hidden']);
      $('#navbarAlert').removeAttr('style');
      $('#navbarAlert').addClass(alert_type);
      $('#navbarAlert').show();
    },
    hide_navbar_alert: function(alert_type = 'alert-success') {
      $('#navbarAlert span').attr('aria-hidden', 'false');
      $('#navbarAlert').removeClass('hidden');
      $('#navbarAlert').hide();
    },
    toggle_navbar_alert: function(alert_type = 'alert-success') {
      var hidden = $('#navbarAlert span').attr('aria-hidden');
      if (hidden == 'true') {
        $('#navbarAlert span').attr('aria-hidden', 'false');
        $('#navbarAlert').removeAttr('style');
        $('#navbarAlert').removeClass('hidden');
        $('#navbarAlert').addClass(alert_type);
        $('#navbarAlert').show();
      } else {
        $('#navbarAlert span').attr('aria-hidden', 'true');
        $('#navbarAlert').addClass([alert_type, 'hidden']);
        $('#navbarAlert').hide();
      }
    },
    render_registration_form: function() {
      this.errors.clear();
      this.form.flag.sign_in = true;
      this.form.flag.password = true;
      this.form.flag.password_confirm = true;
      this.form.submit_text = "Sign Up";
    },
    render_sign_in_form: function() {
      this.errors.clear();
      this.form.flag.sign_in = false;
      this.form.flag.email = true;
      this.form.flag.password = true;
      this.form.flag.password_confirm = false;
      this.form.submit_text = "Sign In";
    },
    render_reset_password_form: function() {
      this.errors.clear();
      this.form.flag.sign_in = true;
      this.form.flag.email = true;
      this.form.flag.password = false;
      this.form.flag.password_confirm = false;
      this.form.submit_text = "Reset Password";
    },
    sign_out: function() {
      let context = this;
      this.axios.delete('/api/v1/auth/logout').then(function(response) {
        context.show_navbar_alert('You are signed out!', 'alert-success');
        context.store.commit('setAccessToken', '');
        context.store.commit('setUserLoggedInFlag', false);
        window.location.reload();
      }).catch(function() {
        window.location.reload();
      });
    },
    submit_user_update_form: function() {
      let context = this;

      if (this.form.input.email && (!this.form.input.password && !this.form.input.password_confirm)) {
        var data = {'email': this.form.input.email};
      }

      if ((this.form.input.password && this.form.input.password_confirm) && !this.form.input.email) {
        var data = {
          'password': this.form.input.password,
          'password_confirm': this.form.input.password_confirm,
        };
      }

      if (this.form.input.password && this.form.input.password_confirm && this.form.input.email) {
        var data = {
          'email': this.form.input.email,
          'password': this.form.input.password,
          'password_confirm': this.form.input.password_confirm,
        };
      }

      this.axios.patch('/api/v1/user/update', data)
        .then(function(response) {
          context.show_navbar_alert('Update succeeded. Reloading page!', 'alert-success');
          this.sign_out();
        }).catch(function(response) {
          if (response.response.data.errors.email) {
            context.errors.add({
              id: 'email',
              field: 'email',
              msg: response.response.data.errors.email[0],
            });
            $('.navbar-nav.ml-auto form input[type="email"]').attr('aria-invalid', true);
            $('.navbar-nav.ml-auto form input[type="email"]').addClass('is-invalid');
          }

          if (response.response.data.errors.password) {
            context.errors.add({
              id: 'password',
              field: 'password',
              msg: response.response.data.errors.password[0],
            });
            $('.navbar-nav.ml-auto form input[type="email"]').attr('aria-invalid', true);
            $('.navbar-nav.ml-auto form input[type="email"]').addClass('is-invalid');
          }
        });

    },
    submit_user_not_logged_in_form: function() {
      let context = this;

      if (this.form.submit_text == 'Sign In') {
        this.axios.post('/api/v1/auth/login', {
          email: this.form.input.email,
          password: this.form.input.password,
        }).then(function(response) {
          context.show_navbar_alert('You are logged in!', 'alert-success');
          context.store.commit('setUserLoggedInFlag');
          window.location.reload();
        }).catch(function(response) {
          if (response.response.data.errors.email) {
            context.errors.add({
              id: 'email',
              field: 'email',
              msg: response.response.data.errors.email[0],
            });
            $('.navbar-nav.ml-auto form input[type="email"]').attr('aria-invalid', true);
            $('.navbar-nav.ml-auto form input[type="email"]').addClass('is-invalid');
          }

          if (response.response.data.errors.password) {
            context.errors.add({
              id: 'password',
              field: 'password',
              msg: response.response.data.errors.password[0],
            });
            $('.navbar-nav.ml-auto form input[type="password"]').attr('aria-invalid', true);
            $('.navbar-nav.ml-auto form input[type="password"]').addClass('is-invalid');
          }

          if (!response.response.data.errors.email && !response.response.data.errors.password) {
            context.show_navbar_alert('Sign In failed.', 'alert-danger');
          }

        });
      }

      if (this.form.submit_text == 'Sign Up') {
        this.axios.post('/api/v1/user/sign_up', {
          email: this.form.input.email,
          password: this.form.input.password,
          password_confirm: this.form.input.password_confirm,
          accept_tnc: this.form.input.accept_tnc
        }).then(function(response) {
          context.show_navbar_alert(`You've been registered. Please check your email.`);
        }).catch(function(response) {
          context.hide_navbar_alert();
          if (response.response.data.errors.email) {
            context.errors.add({
              id: 'email',
              field: 'email',
              msg: response.response.data.errors.email[0],
            });
          } else {
            context.show_navbar_alert('Something went wrong while signing up.', 'alert-danger');
          }
        });
      }

      if (this.form.submit_text == 'Reset Password') {
        this.axios.post('/api/v1/user/reset_password', {
          email: this.form.input.email
        }).then(function(response) {
          context.show_navbar_alert('New password was sent to you. Please check your email.');
        }).catch(function(response) {
          context.hide_navbar_alert();
          if (response.response.data.errors.email) {
            context.errors.add({
              id: 'email',
              field: 'email',
              msg: response.response.data.errors.email[0],
            });
          } else {
            context.show_navbar_alert('Something went wrong while resetting password.', 'alert-danger');
          }
        });
      }
    },
  }
});
