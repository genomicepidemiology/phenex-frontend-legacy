"use strict"

class Regex {

  constructor(grammar) {
    if (grammar) {
      this.grammar = grammar;
    } else {
      this.grammar = {
        email: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      }
    }
  }

  is_valid(arg, re) {
    return re.test(arg);
  }

  is_valid_email(arg, re = this.grammar.email) {
    return re.test(arg.toLowerCase());
  }

}

export default new Regex();
