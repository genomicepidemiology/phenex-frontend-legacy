/*
 * This calls is facilitator to Application storage.
 * That includes easy access to Cookies and LocalStorage and Session
 */

class Storage {

  constructor() {}

  static getCookie(name) {
    var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
    if (match) return match[2];
  }

  static deleteCookie(name, domain='') {
    if (document.cookie.includes(`${name}=`)) {
      document.cookie = `${name}= ; expires = Thu, 01 Jan 1970 00:00:00 GMT; domain=${domain};`;
      return true;
    }
    return false;
  }

}

export default Storage;
